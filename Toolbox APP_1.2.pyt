############################################
## PYTHON TOOLBOX PARA CALCULO DE APP E   ##
## FAIXAS DE RECUPERACAO OBRIGATORIA      ##
## SEGUNDO NOVO CODIGO FLORESTAL          ##
## por Juan Doblas Prieto                 ##
## (c) Instituto Socioambiental, 2016     ##
############################################

## Changelog:
## v1.2 - 10/11/16
## - Faz parámetros de hidrografia opcionais
## - Desliga parametro hidrografia dupla aberta se nao tiver hidrografia dupla
## - Retira defaults de parametros
## v1.1 - 09/11/16
## - Implementa veredas (para computar alagadizos)
## - Assimila Lagos artificiais a Lagos naturais
## v1.0 - 01/11/16
## - Calcula largura de hidrografias duplas
## - Diversas otimizacoes

import arcpy, os, time, math

class Toolbox(object):
    def __init__(self):
        self.label = "Ferramentas de Calculo de APPs"
        self.alias = "ComputeAPP"
        # List of tool classes associated with this toolbox
        self.tools = [PreparaHidroDupla,CalculaAPPeFaixa]

class PreparaHidroDupla(object):
    def __init__(self):
        self.label = "Prepara Hidro Dupla"
        self.description = "Ferramenta para calcular a largura "+ \
                            "de rios de margem dupla" 
        self.canRunInBackground = False
    def getParameterInfo(self):
        hidrPoligono=arcpy.Parameter(
            displayName="Hidrografia bifilar como poligono",
            name="hidrPoligono",
            datatype="GPFeatureLayer",
            parameterType="Required",
            direction="Input")
        hidrPoligonoAberta=arcpy.Parameter(
            displayName="Saida: hidrografia bifilar como linha aberta com distancia",
            name="hidrPoligonoAberta",
            datatype="GPFeatureLayer",
            parameterType="Required",
            direction="Output")
        hidrPoligonoAberta.value="hidro_bifilar_linha_distancia"
        params=[hidrPoligono,hidrPoligonoAberta]
        return params
    def isLicensed(self):
        """Set whether tool is licensed to execute."""
        return True
    def updateParameters(self, parameters):
        """Modify the values and properties of parameters before internal
        validation is performed.  This method is called whenever a parameter
        has been changed."""
        return
    def updateMessages(self, parameters):
        """Modify the messages created by internal validation for each tool
        parameter.  This method is called after internal validation."""
        return
    def execute(self, parameters, messages):
        # Vamos trabalhar em memoria
        arcpy.env.workspace ="in_memory"     
        # Vamos sobreescrever
        arcpy.env.overwriteOutput=True
        hidro_dupla=parameters[0].valueAsText
        saida_final=parameters[1].valueAsText
        #saida_final=pastaTemp+"\\saida_hidro_dupla_com_largura.shp"

        #hidro_dupla="test_hidro_dupla"
        hidro_dupla_dissolve="test_hidro_dupla_dissolve"
        hidro_dupla_splitline=saida_final
        saida_linhas_normais="saida_linhas_normais"
        saida_linhas_clipadas="saida_linhas_clipadas"

        # Prepara: faz dissolve da hidrografia e separa ela em segmentos
        arcpy.Dissolve_management(hidro_dupla,hidro_dupla_dissolve)
        arcpy.SplitLine_management(hidro_dupla_dissolve,hidro_dupla_splitline)
        # Calcula ponto medio e angulo normal de cada segmento
        arcpy.AddField_management(hidro_dupla_splitline, "x1", "DOUBLE", "", "", "", "", "NULLABLE", "NON_REQUIRED", "")
        arcpy.AddField_management(hidro_dupla_splitline, "y1", "DOUBLE", "", "", "", "", "NULLABLE", "NON_REQUIRED", "")
        arcpy.AddField_management(hidro_dupla_splitline, "x2", "DOUBLE", "", "", "", "", "NULLABLE", "NON_REQUIRED", "")
        arcpy.AddField_management(hidro_dupla_splitline, "y2", "DOUBLE", "", "", "", "", "NULLABLE", "NON_REQUIRED", "")
        arcpy.AddField_management(hidro_dupla_splitline, "xm", "DOUBLE", "", "", "", "", "NULLABLE", "NON_REQUIRED", "")
        arcpy.AddField_management(hidro_dupla_splitline, "ym", "DOUBLE", "", "", "", "", "NULLABLE", "NON_REQUIRED", "")
        arcpy.AddField_management(hidro_dupla_splitline, "n_angle", "DOUBLE", "", "", "", "", "NULLABLE", "NON_REQUIRED", "")
        arcpy.AddField_management(hidro_dupla_splitline, "id", "LONG", "", "", "", "", "NULLABLE", "NON_REQUIRED", "")
        arcpy.CalculateField_management(hidro_dupla_splitline, "id","!OBJECTID!","PYTHON_9.3")
        arcpy.CalculateField_management(hidro_dupla_splitline, "x1","!SHAPE.FIRSTPOINT.X!","PYTHON_9.3")
        arcpy.CalculateField_management(hidro_dupla_splitline, "y1","!SHAPE.FIRSTPOINT.Y!","PYTHON_9.3")
        arcpy.CalculateField_management(hidro_dupla_splitline, "x2","!SHAPE.LASTPOINT.X!","PYTHON_9.3")
        arcpy.CalculateField_management(hidro_dupla_splitline, "y2","!SHAPE.LASTPOINT.Y!","PYTHON_9.3")
        arcpy.CalculateField_management(hidro_dupla_splitline, "xm","!SHAPE.CENTROID.X!","PYTHON_9.3")
        arcpy.CalculateField_management(hidro_dupla_splitline, "ym","!SHAPE.CENTROID.Y!","PYTHON_9.3")
        normal_angle_expression="math.atan2(!x1!-!x2!,!y2!-!y1!)"
        arcpy.CalculateField_management(hidro_dupla_splitline, "n_angle",normal_angle_expression,"PYTHON_9.3")
        # Cria linhas normais passando pelo ponto medio de cada segmento
        long_line=10000 #10km de cada lado
        x1_expr="!xm!-"+str(long_line)+"*math.cos(!n_angle!)"
        x2_expr="!xm!+"+str(long_line)+"*math.cos(!n_angle!)"
        y1_expr="!ym!-"+str(long_line)+"*math.sin(!n_angle!)"
        y2_expr="!ym!+"+str(long_line)+"*math.sin(!n_angle!)"
        arcpy.CalculateField_management(hidro_dupla_splitline, "x1",x1_expr,"PYTHON_9.3")
        arcpy.CalculateField_management(hidro_dupla_splitline, "y1",y1_expr,"PYTHON_9.3")
        arcpy.CalculateField_management(hidro_dupla_splitline, "x2",x2_expr,"PYTHON_9.3")
        arcpy.CalculateField_management(hidro_dupla_splitline, "y2",y2_expr,"PYTHON_9.3")
        arcpy.XYToLine_management(hidro_dupla_splitline,saida_linhas_normais,"x1","y1","x2","y2","","id")
        # Calcula o ponto medio da linha normal, vai servir pra depois
        arcpy.AddField_management(saida_linhas_normais, "lxm", "DOUBLE", "", "", "", "", "NULLABLE", "NON_REQUIRED", "")
        arcpy.AddField_management(saida_linhas_normais, "lym", "DOUBLE", "", "", "", "", "NULLABLE", "NON_REQUIRED", "")
        arcpy.CalculateField_management(saida_linhas_normais, "lxm","!SHAPE.CENTROID.X!","PYTHON_9.3")
        arcpy.CalculateField_management(saida_linhas_normais, "lym","!SHAPE.CENTROID.Y!","PYTHON_9.3")

        # Clipa linhas para determinar largura do rio no trecho
        arcpy.Clip_analysis(saida_linhas_normais,hidro_dupla,"saida_linhas_clipadas")

        # Rotina para controlar linhas que cortam duas vezes o rio
        # Primeiro separa as linhas normais clipadas, caso alguma cruce duas vezes o rio vai ser separada em duas linhas
        arcpy.MultipartToSinglepart_management("saida_linhas_clipadas","saida_linhas_clipadas_separadas")
        # Ahora calcula as coordenadas do primeiro ponto das linhas normais separadas
        arcpy.AddField_management("saida_linhas_clipadas_separadas", "lx1", "DOUBLE", "", "", "", "", "NULLABLE", "NON_REQUIRED", "")
        arcpy.AddField_management("saida_linhas_clipadas_separadas", "ly1", "DOUBLE", "", "", "", "", "NULLABLE", "NON_REQUIRED", "")
        arcpy.AddField_management("saida_linhas_clipadas_separadas", "valido", "LONG", "", "", "", "", "NULLABLE", "NON_REQUIRED", "")
        arcpy.AddField_management("saida_linhas_clipadas_separadas", "largura", "DOUBLE", "", "", "", "", "NULLABLE", "NON_REQUIRED", "")
        arcpy.CalculateField_management("saida_linhas_clipadas_separadas", "lx1","!SHAPE.FIRSTPOINT.X!","PYTHON_9.3")
        arcpy.CalculateField_management("saida_linhas_clipadas_separadas", "ly1","!SHAPE.FIRSTPOINT.Y!","PYTHON_9.3")
        arcpy.CalculateField_management("saida_linhas_clipadas_separadas", "largura","!SHAPE.LENGTH!","PYTHON_9.3")
        # O primeiro ponto da linha normal separada tem que ser igual ao ponto medio da linha normal original 
        expr_validacao="(int(!lx1!-!lxm!)==0)*(int(!ly1!-!lym!)==0)"
        arcpy.CalculateField_management("saida_linhas_clipadas_separadas", "valido",expr_validacao,"PYTHON_9.3")
        # Exporta linhas validas
        delimitedField = arcpy.AddFieldDelimiters("saida_linhas_clipadas_separadas","valido")
        expression = delimitedField + " = 1"
        arcpy.FeatureClassToFeatureClass_conversion("saida_linhas_clipadas_separadas",arcpy.env.workspace,"saida_linhas_clipadas_separadas_validas",expression)
        # Join final
        arcpy.JoinField_management(hidro_dupla_splitline,"OBJECTID","saida_linhas_clipadas_separadas_validas","id",["largura"])
        return
      
class CalculaAPPeFaixa(object):
    def __init__(self):
        self.label = "Calcula APP e Faixas"
        self.description = "Ferramenta para calcular APP "+ \
                            "e faixas de recomposicao segundo" + \
                            " o novo Codigo Florestal."+\
                            " Atencao: todas os dados espaciais utilizados " +\
                            "devem estar em integrados em formato Geodatabase e "+\
                            "devem usar sistemas de referencia geograficos "+\
                            "projetados, como UTM, Policonica, Sinusoidal, etc."
        self.canRunInBackground = False

    def getParameterInfo(self):
        Modulo=arcpy.Parameter(
            displayName="Modulo fiscal do municipio (em hectares)",
            name="Modulo",
            datatype="Long",
            parameterType="Required",
            direction="Input")
        Modulo.value=80
        LarguraRioUnifilar=arcpy.Parameter(
            displayName="Largura de rio unifilar (largura total em metros)",
            name="LarguraRioUnifilar",
            datatype="Long",
            parameterType="Required",
            direction="Input")
        LarguraRioUnifilar.value=8
        shapeCAR=arcpy.Parameter(
            displayName="Conjunto de CAR a ser processado (com campo area_ha)",
            name="shapeCAR",
            datatype="GPFeatureLayer",
            parameterType="Required",
            direction="Input")
        #shapeCAR.value="car"
        shapeCAR.filter.list=["Polygon"]
        hidrPoligono=arcpy.Parameter(
            displayName="Hidrografia bifilar como poligono",
            name="hidrPoligono",
            datatype="GPFeatureLayer",
            parameterType="Optional",
            direction="Input")
        #hidrPoligono.value="hidro_bifilar"
        hidrPoligono.filter.list=["Polygon"]
        hidrPoligonoAberta=arcpy.Parameter(
            displayName="Hidrografia bifilar segmentada com largura (saida do modulo de preparacao)",
            name="hidrPoligonoAberta",
            datatype="GPFeatureLayer",
            parameterType="Optional",
            direction="Input")
        #hidrPoligonoAberta.value="hidro_bifilar_linha"
        hidrPoligonoAberta.filter.list=["Polyline"]
        hidrUnifilar=arcpy.Parameter(
            displayName="Hidrografia unifilar",
            name="hidrUnifilar",
            datatype="GPFeatureLayer",
            parameterType="Optional",
            direction="Input")
        #hidrUnifilar.value="hidro_unifilar"
        hidrUnifilar.filter.list=["Polyline"]
        nascentes=arcpy.Parameter(
            displayName="Nascentes",
            name="nascentes",
            datatype="GPFeatureLayer",
            parameterType="Optional",
            direction="Input")
        #nascentes.value="nascente"
        nascentes.filter.list=["Point"]
        lagosNaturais=arcpy.Parameter(
            displayName="Lagos naturais (com campo area_ha)",
            name="lagosNaturais",
            datatype="GPFeatureLayer",
            parameterType="Optional",
            direction="Input")
        #lagosNaturais.value="lago_lagoa_natural_maior1ha"
        lagosNaturais.filter.list=["Polygon"]
        lagosArtif=arcpy.Parameter(
            displayName="Lagos artificiais (com area_ha)",
            name="lagosArtif",
            datatype="GPFeatureLayer",
            parameterType="Optional",
            direction="Input")
        #lagosArtif.value="reservatorio_artificial_maior1ha"
        lagosArtif.filter.list=["Polygon"]
        veredas=arcpy.Parameter(
            displayName="Veredas",
            name="veredas",
            datatype="GPFeatureLayer",
            parameterType="Optional",
            direction="Input")
        #veredas.value="veredas"
        veredas.filter.list=["Polygon"]
        saida_faixa_novo_shp = arcpy.Parameter(
            displayName="Faixas de restauracao obrigatoria calculada",
            name="saida_faixas_novo_shp",
            datatype="GPFeatureLayer",
            parameterType="Required",
            direction="Output")
        #saida_faixa_novo_shp.value="saida_faixa"
        saida_app_novo_shp = arcpy.Parameter(
            displayName="APP calculada",
            name="saida_app_novo_shp",
            datatype="GPFeatureLayer",
            parameterType="Required",
            direction="Output")
        #saida_app_novo_shp.value="saida_app"
        params=[Modulo,LarguraRioUnifilar,shapeCAR,hidrPoligono,
                hidrPoligonoAberta,hidrUnifilar,nascentes,
                lagosNaturais,lagosArtif,veredas,saida_faixa_novo_shp,
                saida_app_novo_shp]
        return params

    def isLicensed(self):
        """Set whether tool is licensed to execute."""
        return True

    def updateParameters(self, parameters):
        """Modify the values and properties of parameters before internal
        validation is performed.  This method is called whenever a parameter
        has been changed."""
        # se o usuario seleciona uma hidrografia dupla,  
        #   ativa o parametro de hidrografia dupla aberta

        if parameters[3].value: 
            parameters[4].enabled = True
        else:
            parameters[4].enabled = False
    
        return

    def updateMessages(self, parameters):
        """Modify the messages created by internal validation for each tool
        parameter.  This method is called after internal validation."""
        return

    def execute(self, parameters, messages):
        
        area=1                                                  
        # parametros APPs e Faixas
        faixaNascentes=15
        APPNascentes=50
        # Vamos sobreescrever
        arcpy.env.overwriteOutput=True
        default_gdb=arcpy.env.workspace
        pastaBase = sys.path[0]
        pastaTemp=arcpy.env.scratchFolder
        GDBTemp=arcpy.env.scratchGDB
        arcpy.AddMessage("Default GDB: "+default_gdb)
        #arcpy.env.workspace=pastaTemp
        arcpy.env.workspace ="in_memory"
        arcpy.AddMessage("Workspace: "+arcpy.env.workspace)
        
        # configura acesso a arquivos de dados adaptando para antigo script
        Modulo=parameters[0].valueAsText
        LarguraRioUnifilar=parameters[1].valueAsText
        Shape_todos_CAR_com_area_ha=parameters[2].valueAsText
        hidr_linha_dupla_aberta=parameters[4].valueAsText
        hidrografia_bifilar_poligonos=parameters[3].valueAsText
        Hidrografia_Unifilar=parameters[5].valueAsText
        nascente_shp=parameters[6].valueAsText
        Lagos_naturais_Maiores_de_1_ha=parameters[7].valueAsText  
        lagos_artificiais_nao_isolados_maiores_de_1_ha_com_area_ha_=parameters[8].valueAsText
        veredas=parameters[9].valueAsText
        saida_faixa_novo_shp=parameters[10].valueAsText
        saida_app_novo_shp=parameters[11].valueAsText
#        saida_app_novo_shp=pastaTemp+"\\saida_app_novo.shp"
#        saida_faixa_novo_shp=pastaTemp+"\\saida_faixa_novo.shp"
        # parametros APPs e Faixas
        faixaNascentes=15
        APPNascentes=50
        # inicialização
        area=1                                                  
        codigoBufferLago="""def calculaBuffer(area, Modulo):
            tamanhoMod=area/Modulo
            if tamanhoMod<=1:
                buf=5
            if (tamanhoMod>1) and (tamanhoMod<=2):
                buf=8
            if (tamanhoMod>2) and (tamanhoMod<=4):
                buf=15
            if (tamanhoMod>4):
                buf=30
            return buf"""
        codigoBufferRioFaixa="""def calculaBuffer(area, Modulo, largura):
            tamanhoMod=area/Modulo
            if tamanhoMod<=1:
                buf=5
            if (tamanhoMod>1) and (tamanhoMod<=2):
                buf=8
            if (tamanhoMod>2) and (tamanhoMod<=4):
                buf=15
            if (tamanhoMod>4) and (tamanhoMod<=10):
                buf=20
            if (tamanhoMod>10):
                buf=30
            if (tamanhoMod>4) and (tamanhoMod<=10):
                if largura>10:
                    buf=largura/2
                    if buf<30:
                        buf=30
                    if buf>100:
                        buf=100
                if largura<10:
                    buf=20
            if (tamanhoMod>10):
                buf=largura/2
                if buf<30:
                    buf=30
                if buf>100:
                    buf=100              
            return buf"""
        codigoBufferRioAPP="""def calculaBuffer(largura):
            if largura<10:
                buf=30
            if (largura>=10) and (largura<50):
                    buf=50
            if (largura>=50) and (largura<200):
                    buf=100
            if (largura>=200) and (largura<600):
                    buf=200
            if largura>=600:
                    buf=500
            return buf"""

# ============== COMENÇA O PROCESSAMENTO, ITERANDO IMOVEL POR IMOVEL ===========

        arcpy.AddMessage("Comecando processamento")
        start_proc=time.clock()
        count_iter=0 
        APP_comecou=False
        faixa_comecou=False
        cursor = arcpy.SearchCursor(Shape_todos_CAR_com_area_ha)
        delimitedField = arcpy.AddFieldDelimiters(Shape_todos_CAR_com_area_ha, "OBJECTID")
        for row in cursor:
            start_CAR=time.clock()
            count_iter=count_iter+1
            # Preparação: cria featureclass com um unico CAR
            arcpy.AddMessage(" Selecionando CAR...")
            numero_CAR=row.getValue("OBJECTID")
            expression = delimitedField + " = "+str(numero_CAR)
            arcpy.FeatureClassToFeatureClass_conversion(Shape_todos_CAR_com_area_ha,"in_memory","CAR_tmp",expression)
            Shape_CAR_com_area_ha="CAR_tmp"
            arcpy.AddMessage("Processando CAR #"+str(count_iter))
            area=row.getValue("area_ha")
            arcpy.AddMessage(" Area CAR "+str(area)+" ha")
            # Preparação: Buffer para pegar rios fora do car
            arcpy.Buffer_analysis(Shape_CAR_com_area_ha, "merge_car_analizado_Buffer", "100 Meters", "FULL", "ROUND", "NONE", "", "PLANAR")
            # Buffer para calculo de APP de grandes rios (geram APP de até 500 metros)
            arcpy.Buffer_analysis(Shape_CAR_com_area_ha, "merge_car_analizado_Buffer_500", "500 Meters", "FULL", "ROUND", "NONE", "", "PLANAR")

            # NASCENTES
            if nascente_shp:
                arcpy.AddMessage(" Calculando nascentes...")
                arcpy.Buffer_analysis(Shape_CAR_com_area_ha, "car_buffer_nascentes", APPNascentes, "FULL", "ROUND", "NONE", "", "PLANAR")
                arcpy.Clip_analysis(nascente_shp,"car_buffer_nascentes", "nascentes_em_CAR")            
                #calcula faixa nascentes
                if (int(arcpy.GetCount_management("nascentes_em_CAR").getOutput(0))==0):
                    temNascentes=False
                    arcpy.AddMessage(" Imovel sem nascentes")                
                else:
                    temNascentes=True
                    #calcula APP e faixa nascentes
                    arcpy.Buffer_analysis("nascentes_em_CAR", "nascentes_buffer_faixa", faixaNascentes, "FULL", "ROUND", "NONE", "", "PLANAR")
                    arcpy.Buffer_analysis("nascentes_em_CAR", "nascentes_buffer_APP", APPNascentes, "FULL", "ROUND", "NONE", "", "PLANAR")
                arcpy.AddMessage(" Feito")
            else:
                temNascentes=False
                
            # LAGOS ARTIFICIAIS
            if lagos_artificiais_nao_isolados_maiores_de_1_ha_com_area_ha_:
                arcpy.AddMessage(" Calculando lagos artificiais...")
                arcpy.Clip_analysis(lagos_artificiais_nao_isolados_maiores_de_1_ha_com_area_ha_,"merge_car_analizado_Buffer", "lagos_artif_em_CAR")
                if (int(arcpy.GetCount_management("lagos_artif_em_CAR").getOutput(0))==0):
                    temLagosArtif=False
                else:
                    temLagosArtif=True
                    # Add Field buffer
                    arcpy.AddField_management("lagos_artif_em_CAR", "buffer_fxa", "DOUBLE", "", "", "", "", "NULLABLE", "NON_REQUIRED", "")
                    arcpy.AddField_management("lagos_artif_em_CAR", "buffer_APP", "DOUBLE", "", "", "", "", "NULLABLE", "NON_REQUIRED", "")
                    # Calcula campo buffer de faixa e de APP (são iguais) para lagos artificiais
                    #arcpy.CalculateField_management("lagos_artif_em_CAR", "buffer_fxa", "(!area_ha!<=20)*15+(!area_ha!>20)*100", "PYTHON_9.3", "")    
                    #arcpy.CalculateField_management("lagos_artif_em_CAR", "buffer_APP", "(!area_ha!<=20)*15+(!area_ha!>20)*100", "PYTHON_9.3", "")    
                    # mudança 10/nov: faixas e APP iguais a lagos naturais
                    expressaoLago="calculaBuffer("+str(area)+","+str(Modulo)+")"
                    arcpy.CalculateField_management("lagos_artif_em_CAR", "buffer_fxa", expressaoLago, "PYTHON_9.3", "")    
                    arcpy.CalculateField_management("lagos_artif_em_CAR", "buffer_APP", "(!area_ha!<=20)*50+(!area_ha!>20)*100", "PYTHON_9.3", "")
            else:
                temLagosArtif=False
                
            # LAGOS NATURAIS
            if Lagos_naturais_Maiores_de_1_ha:
                arcpy.AddMessage(" Calculando lagos artificiais...")
                arcpy.Clip_analysis(Lagos_naturais_Maiores_de_1_ha,"merge_car_analizado_Buffer", "lagos_naturais_em_CAR")                                      
                if (int(arcpy.GetCount_management("lagos_naturais_em_CAR").getOutput(0))==0):
                    temLagosNaturais=False
                else:
                    temLagosNaturais=True                
                    # Add Field buffer
                    arcpy.AddField_management("lagos_naturais_em_CAR", "buffer_fxa", "DOUBLE", "", "", "", "", "NULLABLE", "NON_REQUIRED", "")
                    arcpy.AddField_management("lagos_naturais_em_CAR", "buffer_APP", "DOUBLE", "", "", "", "", "NULLABLE", "NON_REQUIRED", "")            
                    # Calcula campo buffer de APP e faixa para lagos naturais
                    expressaoLago="calculaBuffer("+str(area)+","+str(Modulo)+")"
                    arcpy.CalculateField_management("lagos_naturais_em_CAR", "buffer_fxa", expressaoLago, "PYTHON_9.3", codigoBufferLago)
                    arcpy.CalculateField_management("lagos_naturais_em_CAR", "buffer_APP", "(!area_ha!<=20)*50+(!area_ha!>20)*100", "PYTHON_9.3", "")    
            else:
                temLagosNaturais=False
                
            # VEREDAS
            if veredas:
                arcpy.AddMessage(" Calculando veredas...")
                arcpy.Clip_analysis(veredas,"merge_car_analizado_Buffer", "veredas_em_CAR")                                      
                if (int(arcpy.GetCount_management("veredas_em_CAR").getOutput(0))==0):
                    temVeredas=False
                else:
                    temVeredas=True                
                    # Add Field buffer
                    arcpy.AddField_management("veredas_em_CAR", "buffer_fxa", "DOUBLE", "", "", "", "", "NULLABLE", "NON_REQUIRED", "")
                    arcpy.AddField_management("veredas_em_CAR", "buffer_APP", "DOUBLE", "", "", "", "", "NULLABLE", "NON_REQUIRED", "")            
                    # Calcula campo buffer de APP e faixa para veredas
                    areaMod=area/float(Modulo)
                    expressaoVereda="(("+str(areaMod)+")<=4)*30+(("+str(areaMod)+")>4)*50"
                    arcpy.CalculateField_management("veredas_em_CAR", "buffer_fxa", expressaoVereda, "PYTHON_9.3", "")
                    arcpy.CalculateField_management("veredas_em_CAR", "buffer_APP", "50", "PYTHON_9.3", "") 
            else:
                temVeredas=False
                
            # Merge
            temLagos=True
            temHidroLagunar=True
            if temLagosArtif and temLagosNaturais:
                arcpy.Merge_management(["lagos_artif_em_CAR","lagos_naturais_em_CAR"], "merge_lagos_em_CAR") 
            elif temLagosArtif:
                arcpy.Merge_management(["lagos_artif_em_CAR"], "merge_lagos_em_CAR") 
            elif temLagosNaturais:
                arcpy.Merge_management(["lagos_naturais_em_CAR"], "merge_lagos_em_CAR") 
            else:
                arcpy.AddMessage(" Imovel sem lagos")
                temLagos=False
            # junta com veredas
            if (temLagosArtif or temLagosNaturais) and temVeredas:
                arcpy.Merge_management(["merge_lagos_em_CAR","veredas_em_CAR"], "merge_lagos_veredas_em_CAR")
            elif temVeredas:
                arcpy.Merge_management(["veredas_em_CAR"], "merge_lagos_veredas_em_CAR")
            elif (temLagosArtif or temLagosNaturais):
                arcpy.Merge_management(["merge_lagos_em_CAR"], "merge_lagos_veredas_em_CAR")
            else:
                arcpy.AddMessage(" Imovel sem lagos nem veredas")
                temHidroLagunar=False
                
                
            # Cria buffer de lagos
            if temHidroLagunar:
                arcpy.Buffer_analysis( "merge_lagos_veredas_em_CAR",  "merge_lagos_veredas_em_CAR_buffered_faixa", "buffer_fxa", "FULL", "ROUND", "NONE", "", "PLANAR")
                arcpy.Buffer_analysis("merge_lagos_veredas_em_CAR",  "merge_lagos_veredas_em_CAR_buffered_APP", "buffer_APP", "FULL", "ROUND", "NONE", "", "PLANAR")
            arcpy.AddMessage(" Feito")
            
            # =============== CALCULA LARGURA RIO DUPLO ====================
            if hidrografia_bifilar_poligonos:
                arcpy.AddMessage(" Clipando rios de margem dupla")
                # Clip
                arcpy.Clip_analysis(hidr_linha_dupla_aberta, "merge_car_analizado_Buffer_500", "hidrografia_bifilar_clipCAR", "")
                arcpy.Clip_analysis(hidrografia_bifilar_poligonos, "merge_car_analizado_Buffer_500", "hidrografia_bifilar_poligono_clipCAR", "")
                # Avalia se o CAR tem hidrografia dupla no interior ou no entorno
                n=int(arcpy.GetCount_management("hidrografia_bifilar_poligono_clipCAR").getOutput(0))
                if n==0:
                    temHidrDupla=False
                    arcpy.AddMessage(" Imovel sem hidrografia dupla")
                else:
                    arcpy.AddMessage(" Numero de trechos de rios de margem dupla incidindo no imovel: "+str(n))
                    temHidrDupla=True
                if temHidrDupla:
                    hidr_linha_dupla_split_c_largura="hidrografia_bifilar_clipCAR"
                    arcpy.AddMessage(" Finalizado clip de rios de margem dupla")
            else:
                temHidrDupla=False
    
            # =============== PROCESSA RIOS UNIFILARES ====================
            if Hidrografia_Unifilar:
                arcpy.AddMessage(" Iniciando calculo APP e faixas de rios unifilares...")            
                # Clipa hidrografia unifilar no CAR ampliado 100 metros
                arcpy.Clip_analysis(Hidrografia_Unifilar, "merge_car_analizado_Buffer", "hidrografia_unifilar_clip_CAR", "")
                # Avalia se o CAR tem hidrografia unifilar no interior
                n=int(arcpy.GetCount_management("hidrografia_unifilar_clip_CAR").getOutput(0))
                if n==0:
                    temHidrUnifilar=False
                    arcpy.AddMessage(" Imovel sem hidrografia unifilar")
                else:
                    temHidrUnifilar=True
                    # Buffer para criar rios duplos a partir dos unifilares
                    meiaLargura=int(LarguraRioUnifilar)/2
                    arcpy.Buffer_analysis("hidrografia_unifilar_clip_CAR", "hidrografia_unifilar_buffer", str(meiaLargura), "FULL", "FLAT", "NONE", "", "PLANAR")
                    # Process: Polygon To Line
                    arcpy.PolygonToLine_management("hidrografia_unifilar_buffer", "hidrografia_unifilar_buffer_line", "IDENTIFY_NEIGHBORS")
                    # Process: Split Line At Vertices
                    arcpy.SplitLine_management("hidrografia_unifilar_buffer_line", "hidrografia_unifilar_buffer_line_split")
                    arcpy.AddField_management("hidrografia_unifilar_buffer_line_split", "largura", "DOUBLE", "", "", "", "", "NULLABLE", "NON_REQUIRED", "")
                    # Process: Calculate Field
                    arcpy.CalculateField_management("hidrografia_unifilar_buffer_line_split", "largura",str(LarguraRioUnifilar) , "PYTHON_9.3", "")
                arcpy.AddMessage(" Feito")
            else:
                temHidrUnifilar=False
            
            if temHidrDupla or temHidrUnifilar or temHidroLagunar:
                arcpy.AddMessage(" Criando faixas e APP de hidrografia")
                # Cria shape conjunto de hidrografia bifilar e unifilar, como poligono e como linha
                if (temHidrUnifilar and temHidrDupla):
                    arcpy.Merge_management(["hidrografia_unifilar_buffer","hidrografia_bifilar_poligono_clipCAR"],"hidrografia_poligono_buffer_merge")
                    arcpy.Merge_management(["hidrografia_bifilar_clipCAR","hidrografia_unifilar_buffer_line_split"], "hidrografia_bifilar_unifilar_buffer_merge")        
                if (temHidrUnifilar and ( not temHidrDupla)):
                    arcpy.Merge_management(["hidrografia_unifilar_buffer"],"hidrografia_poligono_buffer_merge")
                    arcpy.Merge_management(["hidrografia_unifilar_buffer_line_split"], "hidrografia_bifilar_unifilar_buffer_merge")
                if (not(temHidrUnifilar) and temHidrDupla):
                    arcpy.Merge_management(["hidrografia_bifilar_poligono_clipCAR"],"hidrografia_poligono_buffer_merge")
                    arcpy.Merge_management(["hidrografia_bifilar_clipCAR"], "hidrografia_bifilar_unifilar_buffer_merge")
                
                # calcula APP e faixa    
                if (temHidrUnifilar or temHidrDupla):                
                    # Process: intersect, hidro em imoveis
                    # arcpy.Clip_analysis("hidrografia_bifilar_unifilar_buffer_merge.shp","merge_car_analizado_Buffer.shp", "hidrografia_c_largura_em_CAR.shp")

                    # Process: Add Field buffer (2)
                    arcpy.AddField_management("hidrografia_bifilar_unifilar_buffer_merge", "bufferFxa", "DOUBLE", "", "", "", "", "NULLABLE", "NON_REQUIRED", "")
                    arcpy.AddField_management("hidrografia_bifilar_unifilar_buffer_merge", "bufferAPP", "DOUBLE", "", "", "", "", "NULLABLE", "NON_REQUIRED", "")
                    # Process: Calcula campo buffer de app e faixas
                    expressaoRioFaixa="calculaBuffer("+str(area)+","+str(Modulo)+",!largura!)"
                    expressaoRioAPP="calculaBuffer(!largura!)"
                    arcpy.CalculateField_management("hidrografia_bifilar_unifilar_buffer_merge", "bufferFxa", expressaoRioFaixa, "PYTHON_9.3", codigoBufferRioFaixa)
                    arcpy.CalculateField_management("hidrografia_bifilar_unifilar_buffer_merge", "bufferAPP", expressaoRioAPP, "PYTHON_9.3", codigoBufferRioAPP)
                                             
                    # Cria APP e faixas bifilar 
                    arcpy.Buffer_analysis("hidrografia_bifilar_unifilar_buffer_merge", "hidrografia_c_largura_em_CAR_buffered_APP", "bufferAPP", "FULL", "ROUND", "NONE", "", "PLANAR")
                    arcpy.Buffer_analysis("hidrografia_bifilar_unifilar_buffer_merge", "hidrografia_c_largura_em_CAR_buffered_faixa", "bufferFxa", "FULL", "ROUND", "NONE", "", "PLANAR")

            # PROCESSAMENTO FINAL
            arcpy.AddMessage (" Processando APP e Faixas totais")
            lista_APP=[]
            lista_faixa=[]
            temAPP=True
            temFaixa=True
            # Junta APP e faixas
            if temNascentes:
                lista_APP=["nascentes_buffer_APP"]
                lista_faixa=["nascentes_buffer_faixa"]
            if temHidroLagunar:
                lista_APP.append("merge_lagos_veredas_em_CAR_buffered_APP")
                lista_faixa.append("merge_lagos_veredas_em_CAR_buffered_faixa")
            if temHidrDupla or temHidrUnifilar:
                lista_APP.append("hidrografia_c_largura_em_CAR_buffered_APP")
                lista_faixa.append("hidrografia_c_largura_em_CAR_buffered_faixa")
            if lista_APP==[]:
                temAPP=False
            if lista_faixa==[]:
                temFaixa=False
            if temFaixa or temAPP:    
                # Junta hidrografia
                arcpy.AddMessage ("  Merge de corpos d'agua")
                if temHidroLagunar:
                    temHidro=True                    
                    if temHidrDupla or temHidrUnifilar:
                        arcpy.Merge_management(["merge_lagos_veredas_em_CAR","hidrografia_poligono_buffer_merge"],"hidro_total")
                    else:
                        arcpy.Merge_management(["merge_lagos_veredas_em_CAR"],"hidro_total")
                elif temHidrDupla or temHidrUnifilar:
                    arcpy.Merge_management(["hidrografia_poligono_buffer_merge"],"hidro_total")
                    temHidro=True             
                # Merge final
                arcpy.AddMessage ("  Merge de APP e faixas")                            
                arcpy.Merge_management(lista_APP,"APP_total")
                arcpy.Merge_management(lista_faixa,"faixa_total")
                # Dissolve
                arcpy.AddMessage ("  Dissolve de APP e faixas")                         
                arcpy.Dissolve_management("APP_total", "APP_total_dissolve")
                arcpy.Dissolve_management("faixa_total", "faixa_total_dissolve")                
                # Limpa APP e Faixas fora do imovel
                arcpy.AddMessage ("  Clip nas APP e faixas")                            
                arcpy.Clip_analysis("APP_total_dissolve", Shape_CAR_com_area_ha, "APP_total_clip")
                arcpy.Clip_analysis("faixa_total_dissolve", Shape_CAR_com_area_ha, "faixa_total_clip")
                # Apaga APP e Faixas sobrepostas a hidrografias, nascentes, lagos
                arcpy.AddMessage ("  Erase na hidrografia")                            
                if temHidro:
                    arcpy.Erase_analysis("faixa_total_clip", "hidro_total","faixa_total_final", "")
                    arcpy.Erase_analysis("APP_total_clip", "hidro_total", "APP_total_final", "")
                else:
                    arcpy.Merge_management(["faixa_total_clip"],"faixa_total_clean")
                    arcpy.Merge_management(["APP_total_clip"],"APP_total_clean")
                    
                # TODO: REFAZER DISSOLVE APP E FAIXAS

                # Coloca identificador de CAR
                arcpy.AddMessage (" Identificando CAR")                            
                arcpy.AddField_management("faixa_total_final", "CAR_ID", "LONG", "", "", "", "", "NULLABLE", "NON_REQUIRED", "")
                arcpy.AddField_management("APP_total_final", "CAR_ID", "LONG", "", "", "", "", "NULLABLE", "NON_REQUIRED", "")
                arcpy.CalculateField_management("faixa_total_final", "CAR_ID",numero_CAR , "PYTHON_9.3", "")
                arcpy.CalculateField_management("APP_total_final", "CAR_ID",numero_CAR , "PYTHON_9.3", "")
                
                # Integra as APP e faxias calculadas ao arquivos de resultados
                arcpy.AddMessage (" Integracao nos calculos previos")                            
                # Primeiro APPs:
                if APP_comecou:
                    arcpy.Append_management(["APP_total_final"],saida_app_novo_shp)
                else:
                    arcpy.Merge_management(["APP_total_final"],saida_app_novo_shp)
                    APP_comecou=True
                # Depois faixas
                if faixa_comecou:
                    arcpy.Append_management(["faixa_total_final"],saida_faixa_novo_shp)
                else:
                    arcpy.Merge_management(["faixa_total_final"],saida_faixa_novo_shp)
                    faixa_comecou=True

               
            arcpy.AddMessage("CAR Finalizado em "+str(time.clock()-start_CAR)+ " seg.")
            
        return
